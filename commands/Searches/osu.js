module.exports = {
    usage: 'Searches for osu profile, and returns statistics', 
    aliases: [], 
    dm: false,
    delete: false, 
    togglable: false, 
    cooldown: 5,     
    //The message object, the message arguments(command suffix) and the bot object can be passed
    process: (msg, args, bot) => { 
        return new Promise(resolve => {      
            osuApi.getUser({u: args}).then(user => {
                resolve({message: "```cs\n" +
							"	Username : " + user.name + " \n" +
					  		"	Total pp : " + user.pp["raw"] + " pp\n" +
							"Country Rank : #" + user.pp["countryRank"] + " \n" +
							" Global Rank : #" + user.pp["rank"] + " \n" +
							"	   Level : " + (user.level).substring(0,5) + " \n" +
							"	Accuracy : " + (user.accuracy).substring(0,5) + "% \n" +
							"Ranked Score : " + user.scores["ranked"] + " \n" +
							 "```"});
            });
            
            
            }
        )
    
    }
}