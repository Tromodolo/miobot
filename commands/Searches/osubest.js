module.exports = {
    usage: 'Searches for osu profile, and returns the top 5 scores for that profile.', 
    aliases: [], 
    dm: false,
    delete: false, 
    togglable: false, 
    cooldown: 5,     
    //The message object, the message arguments(command suffix) and the bot object can be passed
    process: (msg, args, bot) => { 
        return new Promise(resolve => {
            var bestplays = "```cs\n";
            osuApi.getUserBest({u: args, limit: 5}).then(scores => {
                for(var i = 0; i < 5 ; i++){
                    if(!(scores[i][0].mods.indexOf("FreeModAllowed") == -1)){
                        scores[i][0].mods.splice(scores[i][0].mods.indexOf("FreeModAllowed"),1); 
                    }
                    
                    var accuracy = (50 * parseInt(scores[i][0].counts["50"]) + 100 * parseInt(scores[i][0].counts["100"]) + 300 * parseInt(scores[i][0].counts["300"])) / (parseInt(scores[i][0].counts["miss"]) + parseInt(scores[i][0].counts["50"]) + parseInt(scores[i][0].counts["100"]) + parseInt(scores[i][0].counts["300"])) / 3;

                    accuracy = Math.round(accuracy * 100) / 100
                    
                    bestplays += "#" + (i+1) + " " + scores[i][1].title  + " [" +  scores[i][1].version + "] " + scores[i][0].mods +
					           "\n     " + scores[i][0].maxCombo + "x\\" + scores[i][1].maxCombo + "x " + accuracy + "%  " + Math.round(parseFloat(scores[i][0].pp)) + "pp\n";   
                }
                bestplays += "```";
                resolve({message: bestplays });
            });

							
            
            
        

                                
    
    }
)}
}