let getName = require('./../../utils/utils.js').getName;

module.exports = {
    usage: 'Slaps a person like the rude person they are.', 
    aliases: [], 
    dm: false,
    delete: true, 
    togglable: false, 
    cooldown: 5,     
    //The message object, the message arguments(command suffix) and the bot object can be passed
    process: (msg, args, bot) => {         
        return new Promise(resolve => {            
        
            let links = ["http://i.imgur.com/IE2LpmL.gifv", "http://i.imgur.com/E6AWhRw.gifv", "http://i.imgur.com/EsgTBVO.gifv", "http://i.imgur.com/1JqrOkg.gifv", "http://i.imgur.com/ZmLXQSD.gifv", "http://i.imgur.com/mxW4HjT.gifv", "http://i.imgur.com/LRIcBUQ.gifv", "http://i.imgur.com/MJx6BxZ.gifv", "http://i.imgur.com/HDCCHHb.gifv", "http://i.imgur.com/LlYXVef.gifv", "http://i.imgur.com/JTGQ1Nv.jpg", "http://i.imgur.com/hPZ48HB.gifv", "http://i.imgur.com/zIAijLd.gifv"]
            if (args && (msg.mentions || getName(msg, args))) {
                msg.mentions.length === 1 ? user = msg.mentions[0] : user = getName(msg, args).user;
                var message = (user.mention + " was slapped by **" + msg.author.username + "**\n" + links[Math.floor(Math.random() * (links.length))]);
                resolve({message: message});
            } 
            else {
                var message = (bot.user.mention + " was slapped by **" + msg.author.username + "**\n" + links[Math.floor(Math.random() * (links.length))]);
                resolve({message: message});
            }
    }
)}}
