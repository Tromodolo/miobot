let MioPics = reload('./data/MioPics.json');


module.exports = {
    usage: 'Sends a random picture of the one and only waifu Mio', 
    aliases: [], 
    dm: false,
    delete: true, 
    togglable: false, 
    cooldown: 5,     
    //The message object, the message arguments(command suffix) and the bot object can be passed
    process: (msg, args, bot) => {         
        return new Promise(resolve => {            
            
            var miopic = getRandom(1, Object.keys(MioPics).length);
            var miochoice = (msg.author.mention + " You got picture number " + miopic + ":\n " + MioPics[miopic]);  
            resolve({message: miochoice});
        
    }
)}}
