let getName = require('./../../utils/utils.js').getName;

module.exports = {
    usage: 'Brings up the original source image from an emote', 
    aliases: [], 
    dm: false,
    delete: false, 
    togglable: false, 
    cooldown: 5,     
    //The message object, the message arguments(command suffix) and the bot object can be passed
    process: (msg, args, bot) => {         
        return new Promise(resolve => {
            if(args.split(' ').length == 1){
                
                var emoteid = args.substring(args.lastIndexOf(':')+1, args.indexOf('>'));
                var emotelink = "https://cdn.discordapp.com/emojis/" + emoteid + ".png"
                
                if(emoteid != null){
                    resolve({
                        message:emotelink + "\n"
                    });
                }
            }
    }    
)}}
