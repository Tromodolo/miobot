let AnimalJokes = reload('./data/AnimalJokes.json');

module.exports = {
    usage: 'Sends a top quality animal joke', 
    aliases: ['animals'], 
    dm: false,
    delete: false, 
    togglable: false, 
    cooldown: 5,     
    //The message object, the message arguments(command suffix) and the bot object can be passed
    process: (msg, args, bot) => { 
        var jokenumber = getRandom(1, AnimalJokes.jokes.length);
        return new Promise(resolve => {            
            resolve({message: AnimalJokes.jokes[jokenumber]});
        })
    }
}