let getName = require('./../../utils/utils.js').getName;

module.exports = {
    usage: 'Pets a specified user', 
    aliases: [], 
    dm: false,
    delete: false, 
    togglable: false, 
    cooldown: 5,     
    //The message object, the message arguments(command suffix) and the bot object can be passed
    process: (msg, args, bot) => {         
        return new Promise(resolve => {            
        
            let links = ["http://i.imgur.com/Y3GB3K1.gif", "http://i.imgur.com/f7ByidM.gif", "http://i.imgur.com/LUpk6b6.gif"]
            if (args && (msg.mentions || getName(msg, args))) {
                msg.mentions.length === 1 ? user = msg.mentions[0] : user = getName(msg, args).user;
                var message = (user.mention + " was petted by **" + msg.author.username + "**\n" + links[Math.floor(Math.random() * (links.length))]);
                resolve({message: message});
            } 
            else {
                var message = (bot.user.mention + " was petted by **" + msg.author.username + "**\n" + links[Math.floor(Math.random() * (links.length))]);
                resolve({message: message});
            }
    }    
)}}
