let getName = require('./../../utils/utils.js').getName,
    seedrandom = require('seedrandom');

module.exports = {
    usage: 'Estimates love power between two people. Between bot and author if unspecified.', 
    aliases: [], 
    dm: false,
    delete: true, 
    togglable: false, 
    cooldown: 5,     
    //The message object, the message arguments(command suffix) and the bot object can be passed
    process: (msg, args, bot) => {         
        return new Promise(resolve => {
                        
            if(args.split(" ").length == 1){
                var user1 = args.substring(0, args.indexOf(" "));
                user1 = user1.toLowerCase();
                var user2 = bot.user.username;
                user2 = user2.toLowerCase();
                var user1power = 0;
                var user2power = 0;
                for(var i = 0; i < user1.length; i++){

                    user1power += user1.charCodeAt(i);
                }
                for(var i = 0; i < user2.length; i++){

                    user2power += user2.charCodeAt(i);
                }
                var preseed = user1power ^ user2power;              
                var seed = (user1power + user2power + preseed)/3.72 * 1.57 % 32;  //dont ask why i did this specific calculation. I dont know myself.
                var lovepower = seedrandom(seed);
                var lovemessage = ("The love between " + user1 + " and me is: " + Math.round(lovepower()*100) + "%");
                resolve({message: lovemessage});


            }
            else if(args.split(" ").length == 2){
                var user1 = args.substring(0, args.indexOf(" "));
                user1 = args.toLowerCase();
                var user2 = args.split(" ")[1];
                user2 = user2.toLowerCase();
                var user1power = 0;
                var user2power = 0;
                for(var i = 0; i < user1.length; i++){

                    user1power += user1.charCodeAt(i);
                    }
                for(var i = 0; i < user2.length; i++){

                    user2power += user2.charCodeAt(i);
                    }
                var preseed = user1power ^ user2power;              
                var seed = (user1power + user2power + preseed)/3.72 * 1.57 % 32;    
                var lovepower = seedrandom(seed);
                var lovemessage = ("The love between " + user1 + " and " + user2 + " is: " + Math.round(lovepower()*100) + "%");
                resolve({message: lovemessage});

            }
            else{
                var lovemessage = ("Please only specify 1 or 2 people.")
                resolve({message: lovemessage});
                }
        }
                           
           )}
}



/* 
        }*/