let getName = require('./../../utils/utils.js').getName;

module.exports = {
    usage: 'Pokes a person with the might of a thousand men', 
    aliases: [], 
    dm: false,
    delete: false, 
    togglable: false, 
    cooldown: 5,     
    //The message object, the message arguments(command suffix) and the bot object can be passed
    process: (msg, args, bot) => {         
        return new Promise(resolve => {            
        
        
        let randomPoke = Math.random() < 0.5 ? "http://i.imgur.com/J4Vr0Hg.gif" : "http://i.imgur.com/6KpNE1V.gif";
        if (args && (msg.mentions || getName(msg, args) ) ) {
            msg.mentions.length === 1 ? user = msg.mentions[0] : user = getName(msg, args).user;
            var message = user.mention + ", you got poked by " + msg.author.username + "\n " + randomPoke;
            resolve({message: message});
        } 
        else {
            var message = (bot.user.mention + " got poked by **" + msg.author.username + "**\n" + randomPoke);
            resolve({message: message});
        }
        
        
    }
)}
}

/*;
        if (suffix && (msg.mentions || getName(msg, suffix))) {
            msg.mentions.length === 1 ? user = msg.mentions[0] : user = getName(msg, suffix).user;
            bot.createMessage(msg.channel.id, user.mention + " was poked by **" + msg.author.username + "**\n" + randomPoke).catch();
        } else bot.createMessage(msg.channel.id, bot.user.mention + " was poked by **" + msg.author.username + "**\n" + randomPoke).catch(); */