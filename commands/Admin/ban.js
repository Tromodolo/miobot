let getName = require('./../../utils/utils.js').getName;

module.exports = {
    usage: 'Bans a specified user', 
    aliases: [], 
    dm: false,
    delete: true, 
    togglable: false, 
    cooldown: 5,     
    //The message object, the message arguments(command suffix) and the bot object can be passed
    process: (msg, args, bot) => {         
        return new Promise(resolve => {            
        
            if (args && (msg.mentions || getName(msg, args))) {
                msg.mentions.length === 1 ? user = msg.mentions[0] : user = getName(msg, args).user;
                bot.banGuildMember(msg.guild.id, user.id);
                resolve({
                    message: "User " + user.username + " was banned from the server. :hammer:"
                });
            }
            else{
                resolve({
                    message: "Please specify a user"
                });            
            }
            
            
    }    
)}}


