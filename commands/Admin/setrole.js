module.exports = {
    usage: "",
    aliases: [],
    delete: false,
    cooldown: 2,
    process: (msg,args,bot) => {
        return new Promise(resolve => {
            
            var roles = msg.guild.roles;
            
            roles.forEach(function(role) {
                if(role.name == args){
                    pool.query('SELECT autoassignrole FROM guild_settings WHERE guild_id = ' + msg.guild.id, (err, result) => {
                        if(err) console.log(err);
                        else if(result[0] == undefined){
                            pool.query('INSERT into guild_settings SET ?', {guild_id: msg.guild.id, autoassignrole: role.id, rolename: role.name});
                            resolve({message: 'Auto-assign role has been set.'})
                        }
                        else{
                            pool.query('UPDATE guild_settings SET ? WHERE guild_id = ' + msg.guild.id, {autoassignrole: role.id, rolename: role.name}, (err, result) => {
                                if(err) console.log(err);
                                resolve({message: 'Auto-assign role has been updated.'})
                            });
                       }
                    });
                }
            });
        })
    }
}
                           
                          
                        

