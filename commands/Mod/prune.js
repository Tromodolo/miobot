let utils = require('./../../utils/utils.js');

module.exports = {
    usage: 'Prunes a number of messages from a channel.', 
    aliases: [], 
    dm: false,
    delete: false, 
    togglable: false, 
    cooldown: 5,     
    //The message object, the message arguments(command suffix) and the bot object can be passed
    process: (msg, args, bot) => {         
        return new Promise(resolve => {            
            
            if (/^\d+$/.test(args)) {
                if (msg.channel.permissionsOf(bot.user.id).has('manageMessages')) 
                {    
                    msg.channel.purge(parseInt(args)+1).then(count => resolve({
                        message: `Finished pruning  **${count-1}** messages(s) of ${msg.channel.mention}, **${msg.author.username}**-senpai.`,
                        delete: true
                    }));
                };

        } 
        else resolve({message: "Using the prune command requires a number, **" + msg.author.username + "**-senpai."});
        })
    }
}
