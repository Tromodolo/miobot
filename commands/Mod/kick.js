let getName = require('./../../utils/utils.js').getName;

module.exports = {
    usage: 'Kicks a specified user', 
    aliases: [], 
    dm: false,
    delete: true, 
    togglable: false, 
    cooldown: 5,     
    //The message object, the message arguments(command suffix) and the bot object can be passed
    process: (msg, args, bot) => {         
        return new Promise(resolve => {            
        
            if (args && (msg.mentions || getName(msg, args))) {
                msg.mentions.length === 1 ? user = msg.mentions[0] : user = getName(msg, args).user;
                bot.kickGuildMember(msg.guild.id, user.id);
                resolve({
                    message: "User " + user.username + " was kicked from the server. :boot:"
                });
            }
            else{
                resolve({
                    message: "Please specify a user"
                });            
            }
            
            
    }    
)}}


