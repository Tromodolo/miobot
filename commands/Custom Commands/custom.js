let fs = require('fs');


module.exports = {
    usage: "Creates and manipulates Custom Commands.\n\nTo create a command use `custom add [command name] [command content]`\nTo edit a command use `custom edit [command name] [command content]`\nTo remove a command use `custom delete [command name]`\nTo view created commands use `custom`\n",
    aliases: [],
    delete: false,
    cooldown: 2,
    process: (msg,args,bot) => {
        return new Promise(resolve => {
        if(args.split(" ")[0] === "add" || args.split(" ")[0] === "edit" || args.split(" ")[0] === "remove"){
            if(msg.author.id == admins[0] || msg.author.id == admins[1]){
                if(args.split(" ")[0] === "add"){
                    if(!CustomCommands.hasOwnProperty(args.split(" ")[1])){
                        if(args.split(" ")[1].indexOf(args.split(" ")[2]) != -1){
                            CustomCommands[(args.split(" ")[1])] = args.substr(args.indexOf(args.split(" ")[2]) + args.split(" ")[1].length); 
                        }
                        else{
                            CustomCommands[(args.split(" ")[1])] = args.substr(args.indexOf(args.split(" ")[2])); 
                        }
                        fs.writeFileSync('./data/CustomCommands.json', JSON.stringify(CustomCommands, null, "\t") , 'utf-8'); 
                        bot.createMessage(msg.channel.id, [":ok_hand: Command successfully added."]);
                        CustomCommands = reload('./data/CustomCommands.json');
                    }
                    else{
                        bot.createMessage(msg.channel.id, ["Command already exists. Please use .custom edit if you want to edit command."]);
                    }
                }
                else if(args.split(" ")[0] === "remove"){
                    if(CustomCommands.hasOwnProperty(args.split(" ")[1])){
                        delete CustomCommands[args.split(" ")[1]];
                        fs.writeFileSync('./data/CustomCommands.json', JSON.stringify(CustomCommands, null, "\t") , 'utf-8'); 
                        bot.createMessage(msg.channel.id, [":ok_hand: Command successfully removed."]);
                        CustomCommands = reload('./data/CustomCommands.json');
                    }
                    else{
                        bot.createMessage(msg.channel.id, ["Command doesn't exist."]);
                    }
                    
                }
                else if(args.split(" ")[0] === "edit"){
                    if(CustomCommands.hasOwnProperty(args.split(" ")[1])){
                        if(args.split(" ")[1].indexOf(args.split(" ")[2]) != -1){
                            CustomCommands[(args.split(" ")[1])] = args.substr(args.indexOf(args.split(" ")[2]) + args.split(" ")[1].length); 
                        }
                        else{
                            CustomCommands[(args.split(" ")[1])] = args.substr(args.indexOf(args.split(" ")[2])); 
                        }
                        fs.writeFileSync('./data/CustomCommands.json', JSON.stringify(CustomCommands, null, "\t") , 'utf-8'); 
                        bot.createMessage(msg.channel.id, [":ok_hand: Command successfully edited."]);
                        CustomCommands = reload('./data/CustomCommands.json');
                        }
                    else{
                        bot.createMessage(msg.channel.id, ["Command doesn't exist."]);
                    }
                }
            
                else{
                    let msgString = "Commands - " + Object.keys(CustomCommands).sort().map(cmd => "`" + cmd + "`").join(", ");
                    bot.createMessage(msg.channel.id, msgString).catch();
                }
            
            }
            
            else{
                bot.createMessage(msg.channel.id, "You don't have permissions to do those commands.");
            }
    
        }
        else{
            let msgString = "Commands - " + Object.keys(CustomCommands).sort().map(cmd => "`" + cmd + "`").join(", ");
            bot.createMessage(msg.channel.id, msgString).catch();
        }
    }
 )}
}