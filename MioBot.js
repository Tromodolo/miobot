const Eris = require('eris'),
      mysql = require('mysql'),
      tablesUnFlipped = ["┬─┬﻿ ︵ /(.□. \\\\)", "┬─┬ノ( º _ ºノ)", "┬─┬﻿ ノ( ゜-゜ノ)", "┬─┬ ノ( ^_^ノ)", "┬──┬﻿ ¯\\\\_(ツ)", "(╯°□°）╯︵ /(.□. \\\\)"],
      osu = require('node-osu'),
      chalk = require('chalk'),
      colour = new chalk.constructor({
        enabled: true});
      

let config = require('./Config.json');
let CommandLoader = require('./utils/CommandLoader.js'),
    processCmd = require('./utils/CommandHandler.js'),
    utils = require('./utils/utils.js'),
    regex,
    customUsers = [],
    bot = new Eris(config.BotToken, {}, {
        getAllUsers: true,
        messageLimit: 0,
        maxShards: 4,
        autoReconnect: true,
        disableEveryone: true,
        disabledEvents: {
            VOICE_STATE_UPDATE: true,
            TYPING_START: true,
            GUILD_EMOJI_UPDATE: true,
            GUILD_INTEGRATIONS_UPDATE: true,
            GUILD_BAN_ADD: true,
            GUILD_BAN_REMOVE: true,
            MESSAGE_UPDATE: true,
            MESSAGE_DELETE: true,
            MESSAGE_DELETE_BULK: true
        }
    });
    reload = require('require-reload');
    CustomCommands = reload('./data/CustomCommands.json'),
    admins = config.owner_id,
    botC = colour.magenta.bold,
    userC = colour.cyan.bold,
    guildC = colour.black.bold,
    channelC = colour.green.bold,
    miscC = colour.blue.bold,
    warningC = colour.yellow.bold,
    errorC = colour.red.bold,
    osuApi = new osu.Api(config.osuApiKey, {
        // baseUrl: sets the base api url (default: https://osu.ppy.sh/api)
        notFoundAsError: true, // Reject on not found instead of returning nothing. (default: true)
        completeScores: true // When fetching scores also return the beatmap (default: false)
    }),
    pool = mysql.createPool({
        connectionLimit: 100,
        host: "localhost",
        port: "3306",
        user: "Tromodolo",
        password: "Mitsuka123",
        database: "miobot"
    });

    
bot.on("ready", () =>{
    console.log(botC(bot.user.username + " is now Ready."));
    console.log('Current # of Commands Loaded: ' + warningC(Object.keys(commands).length));
    console.log("Users: " + userC(bot.users.size) + " | Channels: " + channelC(Object.keys(bot.channelGuildMap).length) + " | Servers: " + guildC(bot.guilds.size));
});
    

bot.on('error', (err) => { console.log(err.toString()) });

bot.on("guildMemberAdd", (guild, member) => { // When a member joins a guild
    pool.query('SELECT welcomechannel FROM guild_settings WHERE guild_id = ' + guild.id, (err, result) => {
        if(err) console.log(err);
    
        var response = `${member.user.mention} just joined this server!`;
        // Make a welcome string with the member mention
        bot.createMessage(result[0].welcomechannel, response);
        //end the response in the guild's default channel
        
        if(true){
            pool.query('SELECT autoassignrole FROM guild_settings WHERE guild_id = ' + guild.id, (err, result) => {
                if(err) console.log(err);
                var assignrole = result[0].autoassignrole;
                member.roles.push(assignrole);
                // Autoassigns role to member
                bot.editGuildMember(guild.id, member.id,{ roles: member.roles });
            });
            
        }
        
        
        
    })
});

bot.on("guildMemberRemove", (guild, member) => { // When a member leaves a guild
    pool.query('SELECT welcomechannel FROM guild_settings WHERE guild_id = ' + guild.id, (err, result) => {
        if(err) console.log(err);
    
        var response = `${member.user.username} just left this server.`;
         //Make a farewell string with the member's username
        bot.createMessage(result[0].welcomechannel, response);
        
        })
});
    


bot.on("messageCreate", msg => {
    if (msg.content === "(╯°□°）╯︵ ┻━┻") bot.createMessage(msg.channel.id, tablesUnFlipped[Math.floor(Math.random() * (tablesUnFlipped.length))]).catch();
    if ((msg.author.bot) || !msg.channel.guild) return;
    if(CustomCommands[msg.content.toString()] != null){
        console.log(guildC("@" + msg.channel.guild.name + ":") + channelC(" #" + msg.channel.name) + ": " + warningC("Custom Command " + msg.content.toString()) + " was used by " + userC(msg.author.username));
        bot.createMessage(msg.channel.id, CustomCommands[msg.content.toString()]);
    }
    else{
        let msgPrefix = config.prefix;
        if (msg.content.startsWith(msgPrefix)) {
            let formatedMsg = msg.content.substring(msgPrefix.length, msg.content.length), //Format message to remove command prefix
                cmdTxt = formatedMsg.split(" ")[0].toLowerCase(), //Get command from the formated message
                args = formatedMsg.substring((formatedMsg.split(" ")[0]).length + 1); //Get arguments from the formated message
            if (commandAliases.hasOwnProperty(cmdTxt)) cmdTxt = commandAliases[cmdTxt]; //If the cmdTxt is an alias of the command
            if (cmdTxt === 'channelmute') processCmd(msg, args, commands[cmdTxt], bot); //Override channelCheck if cmd is channelmute to unmute a muted channel
            //Check if a Command was used and runs the corresponding code depending on if it was used in a Guild or not, if in guild checks for muted channel and disabled command
            else if (commands.hasOwnProperty(cmdTxt)){
                processCmd(msg, args, commands[cmdTxt], bot)
            }
        }
    }
});

bot.on("guildCreate", guild =>{
    
    pool.query('INSERT into guild_settings SET ?', { 
            guild_id: guild.id, 
    });   
    
    bot.createMessage(guild.id, "wah");

});


bot.on("guildDelete", guild =>{
    
    pool.query('DELETE FROM guild_settings WHERE guild_id = ' + guild.id, function (err, result){
                 if (err) throw err;
    });    
    
}); 



CommandLoader.load().then(() => {
    bot.connect().then(console.log(warningC("Logged in using Token"))).catch(err => console.log(errorC(err.stack)));
}).catch(err => errorC(err.stack));

                              
getRandom = function(min, max) {
        return min + Math.floor(Math.random() * (max - min + 1));
        }

checkWelcome = function(guild){
    pool.query('SELECT welcometoggle FROM guild_settings WHERE guild_id = ' + guild.id, (err, result) => {
        if(err) console.log(err);
        console.log(result[0].welcometoggle);
        
    a    }); 
}   


